#include "hbclass.ch"
#include "db/db-type.ch"

CLASS DBTables

EXPORTED:
   METHOD new( cName ) INLINE ::cName := cName, SELF

   METHOD int( cField, nSize )
   METHOD string( cField, nSize )
   METHOD decimal( cField, nSize, nDecimal )
   METHOD date( cField )

   METHOD unique()
   METHOD primaryKey()
   METHOD autoIncrement()
   METHOD notNull()
   METHOD uniqueIndex()

   METHOD create()
   METHOD update()

HIDDEN:

   METHOD sayType( oField )
   METHOD sayUnique( oField )
   METHOD sayPrimaryKey( oField )
   METHOD sayUniqueIndex( oField )
   METHOD sayAutoIncrement( oField )
   METHOD sayNotNull( oField )

   METHOD getFieldPrimaryKey()
   METHOD getFieldUniqueIndex()

   METHOD retYesOrNo( lValue ) INLINE IIf( lValue, "YES", "NO" )

   VAR aFields INIT {}
   VAR cName   INIT ""

ENDCLASS

/******************************************************************************/
METHOD int( cField, nSize ) CLASS DBTables

   AAdd( ::aFields, DBTypeInt():new( cField, nSize ) )

RETURN Self

/******************************************************************************/
METHOD string( cField, nSize ) CLASS DBTables

   AAdd( ::aFields, DBTypeString():new( cField, nSize ) )

RETURN Self

/******************************************************************************/
METHOD decimal( cField, nSize, nDecimal ) CLASS DBTables

   AAdd( ::aFields, DBTypeDecimal():new( cField, nSize, nDecimal ) )

RETURN Self

/******************************************************************************/
METHOD date( cField )

   AAdd( ::aFields, DBTypeDate():new( cField ) )

RETURN Self

/******************************************************************************/
METHOD unique() CLASS DBTables

   LOCAL oLastFieldAdded

   oLastFieldAdded := ATail( ::aFields )
   oLastFieldAdded:unique()

RETURN Self

/******************************************************************************/
METHOD autoIncrement() CLASS DBTables
/* APENAS PARA INT */

   LOCAL oLastFieldAdded

   oLastFieldAdded := ATail( ::aFields )
   oLastFieldAdded:autoIncrement()

RETURN Self

/******************************************************************************/
METHOD primaryKey() CLASS DBTables

   LOCAL oLastFieldAdded

   oLastFieldAdded := ATail( ::aFields )
   oLastFieldAdded:primaryKey()

RETURN Self

/******************************************************************************/
METHOD notNull() CLASS DBTables

   LOCAL oLastFieldAdded

   oLastFieldAdded := ATail( ::aFields )
   oLastFieldAdded:notNull()

RETURN Self

/******************************************************************************/
METHOD uniqueIndex() CLASS DBTables

   LOCAL oLastFieldAdded

   oLastFieldAdded := ATail( ::aFields )
   oLastFieldAdded:uniqueIndex()

RETURN Self

/******************************************************************************/
METHOD sayType( oField ) CLASS DBTables

   LOCAL cType

   SWITCH oField:type()
      CASE __DB_TYPE_INT
         cType := " INTEGER(" + HB_NTOS( oField:nSize ) + ") "
         EXIT
      CASE __DB_TYPE_STRING
         cType := " VARCHAR(" + HB_NToS( oField:nSize ) + ") "
         EXIT
      CASE __DB_TYPE_DECIMAL
         cType := " DECIMAL(" + HB_NToS( oField:nSize ) + "," + HB_NToS( oField:nDecimal ) + ") "
         EXIT
      CASE __DB_TYPE_DATE
         cType := " DATE "
         EXIT
   ENDSWITCH

RETURN cType

/******************************************************************************/
METHOD sayUnique( oField ) CLASS DBTables

   IF oField:isUnique()
      RETURN " UNIQUE "
   ENDIF

RETURN ""

/******************************************************************************/
METHOD sayPrimaryKey( oField ) CLASS DBTables

   IF HB_IsObject( oField ) .AND. oField:isPrimaryKey()
      RETURN ", PRIMARY KEY(`" + oField:cField + "`)"
   ENDIF

RETURN ""

/******************************************************************************/
METHOD sayUniqueIndex( oField ) CLASS DBTables

   IF HB_IsObject( oField ) .AND. oField:isUniqueIndex()
      RETURN ", UNIQUE INDEX `" + oField:cField + "_UNIQUE` (`" + oField:cField + "` ASC)"
   ENDIF

RETURN ""

/******************************************************************************/
METHOD sayAutoIncrement( oField ) CLASS DBTables

   IF oField:type() == __DB_TYPE_INT .AND. oField:isAutoIncrement()
      RETURN " AUTO_INCREMENT "
   ENDIF

RETURN ""

/******************************************************************************/
METHOD sayNotNull( oField ) CLASS DBTables

   IF oField:isNotNull()
      RETURN " NOT NULL "
   ENDIF

RETURN " NULL "

/******************************************************************************/
METHOD getFieldPrimaryKey() CLASS DBTables

   LOCAL oField, x

   FOR x := 1 TO Len( ::aFields )
      IF ::aFields[ x ]:isPrimaryKey()
         oField := ::aFields[x]
         EXIT
      ENDIF
   NEXT

RETURN oField

/*****************************************************************************/
METHOD getFieldUniqueIndex() CLASS DBTables

   LOCAL oField, x

   FOR x := 1 TO Len( ::aFields )
      IF ::aFields[ x ]:isUniqueIndex()
         oField := ::aFields[x]
         EXIT
      ENDIF
   NEXT

RETURN oField

/*****************************************************************************/
METHOD create() CLASS DBTables

   LOCAL cQueryTable, oField

/*
   Monta query da criacao da tabela
*/
   cQueryTable := "CREATE TABLE IF NOT EXISTS " + ::cName + " ( "

   FOR EACH oField IN ::aFields
      IF HB_EnumIndex( oField ) != 1
         cQueryTable += ", "
      ENDIF

      cQueryTable += " `" + oField:cField + "` "
      cQueryTable += ::sayType( oField )
      cQueryTable += ::sayNotNull( oField )
      cQueryTable += ::sayAutoIncrement( oField )
      cQueryTable += ::sayUnique( oField )
   NEXT

   cQueryTable += ::sayPrimaryKey( ::getFieldPrimaryKey() )
   cQueryTable += ::sayUniqueIndex( ::getFieldUniqueIndex() )
   cQueryTable += ")"

   GetConnection():sqlQuery( cQueryTable )

RETURN Self

/******************************************************************************/
METHOD update() CLASS DBTables

/*
   LOCAL oQuery, nCont, hFieldProps, oField

   IF !GetConnection():tableExist( ::cName )
      RETURN Self
   ENDIF

   oQuery := GetConnection():query( "SHOW COLUMNS FROM " + ::cName )

   hFieldProps := HB_Hash()
   nCont := 0
   WHILE !oQuery:eof()
      oField := ::aFields[ ++nCont ]

      IF Lower( AllTrim( oField:cField ) ) != Lower( Alltrim( oQuery:field ) )
         hFieldProps["Field"] := oField:cField
      ENDIF

      IF Lower( Alltrim( ::sayType( oField ) ) ) != Lower( Alltrim( oQuery:type ) )
         hFieldProps["Type"] := ::sayType( oField )
      ENDIF

      IF Lower( ::retYesOrNo( !oField:isNotNull() ) ) != Lower( AllTrim( oQuery:null ) )
         hFieldProps["Null"] := ::retYesOrNo( !oField:isNotNull() )
      ENDIF

      oQuery:skip()
   ENDDO
*/

RETURN Self
