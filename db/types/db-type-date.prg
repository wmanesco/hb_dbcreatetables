#include "hbclass.ch"
#include "db/db-type.ch"

CLASS DBTypeDate FROM IDBType

EXPORTED:
   METHOD new( cField )
   METHOD type() INLINE __DB_TYPE_DATE

   VAR cField INIT ""

ENDCLASS

/******************************************************************************/
METHOD new( cField, nSize ) CLASS DBTypeDate

   ::cField := cField

RETURN Self
