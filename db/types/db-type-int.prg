#include "hbclass.ch"
#include "db/db-type.ch"

CLASS DBTypeInt FROM IDBType

EXPORTED:
   METHOD new( cField, nSize )
   METHOD autoIncrement()   INLINE ::lAutoIncrement := .T.
   METHOD isAutoIncrement() INLINE ::lAutoIncrement

   METHOD type() INLINE __DB_TYPE_INT

   VAR cField INIT ""
   VAR nSize  INIT 11

   VAR lAutoIncrement INIT .F.

ENDCLASS

/******************************************************************************/
METHOD new( cField, nSize ) CLASS DBTypeInt

   IF nSize == NIL
      nSize := 11
   ENDIF

   ::cField := cField
   ::nSize  := nSize

RETURN Self
