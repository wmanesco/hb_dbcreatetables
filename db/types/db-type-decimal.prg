#include "hbclass.ch"
#include "db/db-type.ch"

CLASS DBTypeDecimal FROM IDBType

EXPORTED:
   METHOD new( cField, nSize, nDecimal )
   METHOD type() INLINE __DB_TYPE_DECIMAL

   VAR cField   INIT ""
   VAR nSize    INIT 11
   VAR nDecimal INIT 0

ENDCLASS

/******************************************************************************/
METHOD new( cField, nSize, nDecimal ) CLASS DBTypeDecimal

   ::cField   := cField
   ::nSize    := nSize
   ::nDecimal := nDecimal

RETURN Self
