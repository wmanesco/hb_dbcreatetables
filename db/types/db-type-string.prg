#include "hbclass.ch"
#include "db/db-type.ch"

CLASS DBTypeString FROM IDBType

EXPORTED:
   METHOD new( cField, nSize )
   METHOD type() INLINE __DB_TYPE_STRING

   VAR cField INIT ""
   VAR nSize  INIT 255

ENDCLASS

/******************************************************************************/
METHOD new( cField, nSize ) CLASS DBTypeString

   IF nSize == NIL
      nSize := 255
   ENDIF

   ::cField := cField
   ::nSize  := nSize

RETURN Self
