#include "hbclass.ch"

CLASS IDBType

EXPORTED:

   METHOD primaryKey()    INLINE ::lPrimaryKey  := .T.
   METHOD unique()        INLINE ::lUnique      := .T.
   METHOD notNull()       INLINE ::lNotNull     := .T.
   METHOD UniqueIndex()   INLINE ::lUniqueIndex := .T.

   METHOD isPrimaryKey()  INLINE ::lPrimaryKey
   METHOD isUnique()      INLINE ::lUnique
   METHOD isNotNull()     INLINE ::lNotNull
   METHOD isUniqueIndex() INLINE ::lUniqueIndex

HIDDEN:

   VAR lPrimaryKey  INIT .F.
   VAR lUnique      INIT .F.
   VAR lNotNull     INIT .F.
   VAR lUniqueIndex INIT .F.

ENDCLASS
